package linearhashing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import core.Bucket;
import core.DatabaseConstants;
import core.HashTable;
import core.Tuple;

public class LinearHashTableTest {

	private static final double LOAD_THRESHOLD = 0.8;
	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000);
    
    private HashTable hashTable;

    @Before
    public void setUp() throws Exception {
        hashTable = new LinearHashTable();
    }


    @Test
    public void testInitialSize() throws Exception {
        String message = "The LinearHashTable should start with just 2 buckets";
        assertEquals(message, 2, hashTable.numberOfBuckets());
    }

    @Test
    public void testAdd() throws Exception {
    	// Put together a mixture of tuples (comment indicates hash's last 8 bits)
    	Tuple[] tuples = { 
    			new Tuple(0, "Australia", "22 Degrees"), // 1010 0000
    			new Tuple(0, "Brazil", "25 Degrees"),    // 1000 1100
    			new Tuple(0, "China", "7 Degrees"),      // 0011 0111
    			new Tuple(0, "Denmark", "8 Degrees"),    // 1100 0110
    			new Tuple(0, "Egypt", "22 Degrees"),     // 0011 1011
    			new Tuple(0, "France", "11 Degrees"),    // 0011 1011
    			new Tuple(0, "Greece", "15 Degrees")     // 1000 1101     			
    	};
        int nBuckets = hashTable.numberOfBuckets();
        assertEquals("Initial capacity should be just 2 buckets", 2, nBuckets);
        Bucket firstBucket = hashTable.getBucketsForTesting()[0];
        Bucket secondBucket = hashTable.getBucketsForTesting()[1];
        assertNotNull("First page should already exist", firstBucket);
        assertNotNull("Second page should already exist", secondBucket);
        
        // Fill the bucket to just below load threshold
        int n=0; // count of inserts
        int threshold = (int) Math.floor(LOAD_THRESHOLD*nBuckets*DatabaseConstants.BUCKET_CAPACITY);
		while(++n < threshold) {
            hashTable.put(tuples[n % tuples.length]);
            assertEquals("No splitting should occur for insertion of record " + n, nBuckets, hashTable.numberOfBuckets());
        }

        int targetBucketSize = firstBucket.size();
        // Add another to trigger split
        hashTable.put(tuples[n % tuples.length]);
        assertEquals("Extra page should be created when >= 80% capacity", nBuckets+1, hashTable.numberOfBuckets());
        assertTrue("First bucket split", targetBucketSize>firstBucket.size());

        //
        // Repeat for split of bucket 2
        //
        nBuckets = hashTable.numberOfBuckets();
        threshold = (int) Math.floor(LOAD_THRESHOLD*nBuckets*DatabaseConstants.BUCKET_CAPACITY);
        while(++n < threshold) {
            hashTable.put(tuples[n % tuples.length]);
            assertEquals("Second splitting should not occur for insertion of record " + n, nBuckets, hashTable.numberOfBuckets());  
        }
        targetBucketSize = secondBucket.size();
        // Add another to trigger split
        hashTable.put(tuples[n % tuples.length]);
        assertEquals("Second extra page should be created when >= 80% capacity", nBuckets+1, hashTable.numberOfBuckets());
        assertTrue("Second bucket split", targetBucketSize>secondBucket.size());
        
        // Now splitting should start again at page 0
        nBuckets = hashTable.numberOfBuckets();
        threshold = (int) Math.floor(LOAD_THRESHOLD*nBuckets*DatabaseConstants.BUCKET_CAPACITY);
        while(++n < threshold) {
            hashTable.put(tuples[n % tuples.length]);
        }
        assertEquals("Third splitting should not occur before 80% capacity", nBuckets, hashTable.numberOfBuckets());
        targetBucketSize = firstBucket.size();
        // Add another to trigger split
        hashTable.put(tuples[n % tuples.length]);
        assertEquals("Third extra page should be created when >= 80% capacity", nBuckets+1, hashTable.numberOfBuckets());
        assertTrue("First bucket split", targetBucketSize>firstBucket.size());
    }

    @Test
    public void testGetNoSuchElement() {
        String message = "Occurs when it tries to get an element that doesn't exist."
                + "Check that you are returning null when there is no element found";
        assertNull(message, hashTable.get("apples"));
    }

    @Test
    public void testOverflow() {
        Tuple tuple = new Tuple(0, "Australia", "27 Degrees");
        for(int i = 0; i < DatabaseConstants.BUCKET_CAPACITY * 2; i++) {
            hashTable.put(tuple);
        }
        String message = "Make sure you're handing bucket overflows when getting elements correctly";
        assertEquals(message, DatabaseConstants.BUCKET_CAPACITY * 2, hashTable.numberOfTuples());
        Bucket found = hashTable.getBucket(tuple.getKeyHash());
        assertEquals(message, DatabaseConstants.BUCKET_CAPACITY * 2, found.size());
        assertEquals(message, DatabaseConstants.BUCKET_CAPACITY * 2, found.getAll(tuple.getKey()).size());
    }

    @Test
    public void testGetBucket() {
        Bucket first = hashTable.getBucketsForTesting()[0];
        // Add some values
        Tuple tupleOne = new Tuple(0, "Australia", "27 Degrees");
        for(int i = 0; i < DatabaseConstants.BUCKET_CAPACITY * 2; i++) {
            hashTable.put(tupleOne);
        }
        // Test that they were placed in the right buckets
        // Test after global pointer
        Bucket second = hashTable.getBucketsForTesting()[1];
        assertEquals("Buckets after pointer should hash to same bucket", second, hashTable.getBucket(1)); // 0001
        assertEquals("Buckets after pointer should hash to same bucket", second, hashTable.getBucket(3)); // 0011
        // Test before global pointer
        Bucket third = hashTable.getBucketsForTesting()[2];
        assertEquals("Buckets before pointer should hash to different buckets", first, hashTable.getBucket(0)); // 0000
        assertEquals("Buckets before pointer should hash to different buckets", third, hashTable.getBucket(2)); // 0010
    }

}