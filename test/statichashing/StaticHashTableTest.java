package statichashing;

import core.Bucket;
import core.DatabaseConstants;
import core.HashTable;
import core.Tuple;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.HashSet;

import static org.junit.Assert.*;

public class StaticHashTableTest {
	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000);
    
    private HashTable hashTable;

    @Before
    public void setUp() throws Exception {
        hashTable = new StaticHashTable();
    }

    @Test
    public void testInitialSize() throws Exception {
        String message = "Make sure you use DatabaseConstants.HASHTABLE_SIZE as the number of initial buckets for the StaticHashTable";
        assertEquals(message, DatabaseConstants.HASHTABLE_SIZE, hashTable.numberOfBuckets());
    }

    @Test
    public void testAllUnique() throws Exception {
        HashSet<Bucket> setOfBuckets = new HashSet<Bucket>();
        for(Bucket bucket : hashTable.getBucketsForTesting()) {
            setOfBuckets.add(bucket);
        }
        String message = "Make sure you're initialising a new bucket for each slot in the array";
        assertEquals(message, DatabaseConstants.HASHTABLE_SIZE, setOfBuckets.size());
    }

    @Test
    public void testAdd() throws Exception {
        Tuple tuple = new Tuple(0, "Australia", "27 Degrees");
        hashTable.put(tuple);
        Tuple found = hashTable.get(tuple.getKey());
        String message = "Can't get an element from the HashTable. Check that you are using tuple.getKeyHash().";
        assertSame(message, tuple, found);
    }

    @Test
    public void testGetNoSuchElement() {
        String message = "Occurs when it tries to get an element that doesn't exist."
                            + "Check that you are returning null when there is no element found";
        assertNull(message, hashTable.get("apples"));
    }

    @Test
    public void testOverflow() {
        Tuple tuple = new Tuple(0, "Australia", "27 Degrees");
        final int nTuples = DatabaseConstants.BUCKET_CAPACITY * 2;
		for(int i = 0; i < nTuples; i++) {
            hashTable.put(tuple);
        }
        assertEquals("Count of all tuples in table should match number inserted", nTuples, hashTable.numberOfTuples());
        Bucket found = hashTable.getBucket(tuple.getKeyHash());
        assertEquals("All tuples should be in the one bucket (or its overflow buckets) matching the hash key", nTuples, found.size());
        assertEquals("All matching tuples should be findable from the hash key", nTuples, found.getAll(tuple.getKey()).size());
    }
}