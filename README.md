# Week 4 - Hashing Indexes

Hash indexing is ideal for queries where you only want to test equality. 

In this week's task we've provided a minimal set of classes to support the kinds of page-based hash indexing that might be used in a relational DBMS. The code simplifies many of the operations you dealt with last week, so the focus moves away from using the buffer manager and using schemas.

For these tasks, we strongly recommend looking at the lecture slides for this week and the comments in the code to help clarify algorithms you need to implement. As with last week, tests are included in the code to assist you. You may find it helpful to contribute tests of your own on the discussion forum for other students to use.

Otherwise we encourage you to discuss these tasks on the forum. You are more than welcome to share your understanding about how these algorithms work to help each other.

Once again, this week there are three different tasks that you can choose from, ranging from easy, medium and hard difficulty.

1. Easy: Static Hashing
 - Implement the missing methods marked `//TODO` in `src/core/Bucket.java` and `src/core.HashTable/java`
 - Implement common and the `src/statichashing/StaticHashTable.java` file
 - Pass all the tests in `test/statichashing/StaticHashTableTest.java`
 
2. Medium: Extendible Hashing
 - Implement the missing methods marked `//TODO` in `src/core/Bucket.java` and `src/core.HashTable/java`
 - Implement common and the `src/extendiblehashing/ExtendibleHashTable.java` file
 - Pass all the tests in `test/extendiblehashing/ExtendibleHashTableTest.java`

3. Hard: Linear Hashing
 - Implement the missing methods marked `//TODO` in `src/core/Bucket.java` and `src/core.HashTable/java`
 - Implement common and the `src/linearhashing/LinearHashTable.java` file
 - Pass all the tests in `test/linearhashing/LinearHashTableTest.java`

To submit, zip up the `src` folder and upload it to PASTA.

## Linear Hashing

The lecture did not cover Linear Hashing, and you are expected to conduct some independent reading to understand how this works. Some basic slides are included in this repository to get you started. Watch out also for presentations on the topic from your peers.

The trigger for actually splitting buckets is an adjustable detail in Linear Hashing. In this task the trigger should be when the number of tuples stored is 80% of the available space, not including overflow buckets (sometimes referred to as a *load factor* of 80%).

Otherwise we encourage you to discuss these tasks on the forum. You are more than welcome to share your understanding about how these algorithms work to help each other.

