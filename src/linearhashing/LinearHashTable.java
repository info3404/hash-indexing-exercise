package linearhashing;

import core.Bucket;
import core.HashTable;
import core.Tuple;

import java.util.List;

/**
 * LinearHashTable
 * - Expanding size HashTable
 * - Keeps a pointer to the next bucket to split
 * - Keeps track of the current global level. Where size of the array is 2^G * N
 */
public class LinearHashTable extends HashTable {

    protected static final int INITIAL_BUCKETS = 2;
    protected int globalLevel;
    protected int next;

    /**
     * Initialises the buckets, initial number of buckets is INITIAL_BUCKETS, next bucket to split is buckets[0]
     */
    public LinearHashTable() {
        this.buckets = new Bucket[INITIAL_BUCKETS];
        for(int i = 0; i < INITIAL_BUCKETS; i++) {
            buckets[i] = new Bucket();
        }
        this.globalLevel = 0;
        this.next = 0;
    }

    /**
     * If the next bucket to split is greater than or equal to the h_level(hash), then return the h_level+1(hash) bucket
     * If the next bucket to split is less than h_level(hash), then we just return the h_level(hash) bucket
     */
    @Override
    public Bucket getBucket(int hash) {
        if(hash < 0) throw new AssertionError("Hash must be positive");
        return null;
    }

    /**
     * 1. Inserts the entry into the appropriate bucket (you may want to use super.put(entry) to help you.
     * 2. If the HashTable has reached 80% capacity, then we should split the bucket pointed to by *next* and increase the size of the array by 1
     * 3. If necessary, increase the global level of the HashTable
     *
     * Note: If you have done all the split's you can for a certain level (i.e. once buckets.length  == 2*original size of the array for this level)
     *       you will need to reset the next pointer and increase the global level of the hash table
     */
    @Override
    public void put(Tuple entry) {
    }


}
